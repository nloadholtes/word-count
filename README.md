# Running

This was developed against Python 3.6.5 on OSX 10.13.4

After creating a virtualenv, running the app is then just:

`python3 wordcounter/wordcounter.py --num-workers 3 <name of book>`

Where `<name of book>` is the path to a text file containing a book.

Options:

* `--num-workers` Control how many processes are used (default: 3)
* `--read-buffer-size` How large of a buffer should each worker use (default 500 lines)
* `--top-x` How many words should be return in the count at the end



Books tested against:

* The Time Machine: https://www.gutenberg.org/files/35/35-0.txt
* War and Peace: https://www.gutenberg.org/files/2600/2600-0.txt


# Testing

There are two test to check the parsing/tokenization and the counting of those results.

`pytest`