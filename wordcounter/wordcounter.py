from collections import defaultdict
import re
from multiprocessing import Process, Queue, JoinableQueue
from queue import Empty
import argparse

LETTERS_NUMBERS_ONLY = re.compile(r"[^a-zA-Z0-9]")
PROCESSOR_COUNT = 3
TOP_X_WORDS = 10
READ_BUFFER_SIZE = 500

def tokenize(text):
    """Split by words (anything other than [a-zA-Z0-9]"""
    return [w for w in LETTERS_NUMBERS_ONLY.split(text) if w]


def count_unique_words(tokenized_words):
    """Count what is in the array"""
    counter = defaultdict(int)
    for word in tokenized_words:
        word = word.lower()
        value = counter[word]
        counter[word] = value + 1
    return counter


def process_text(input_queue, output_queue):
    """Read the text, tokenize, get the unique words"""
    while True:
        try:
            text = input_queue.get(timeout=4)
            if text is None:
                input_queue.task_done()
                break
            tokens = tokenize(text)
            output_queue.put(count_unique_words(tokens))
            input_queue.task_done()
        except Empty as empty:
            print("EMPTY")
    # print("process_text worker Exiting")
    return


def most_common_words(results_queue, limit=10):
    """Combine all the results, and print out the top X"""
    words = defaultdict(int)
    while not results_queue.empty():
        chunk = results_queue.get()
        for word, count in chunk.items():
            value = words[word]
            words[word] = value + count
    output = [item for item in words.items()]
    output.sort(key=lambda x: x[1])
    output.reverse()
    return output[:limit]


def start_work(book, num_workers, top_x_words, read_buffer_size):
    """Given a book, find out the top X most frequent words"""
    work_queue = JoinableQueue()
    results_queue = Queue()
    worker_pool = []
    for x in range(num_workers):
        p = Process(target=process_text, args=(work_queue, results_queue))
        p.start()
        worker_pool.append(p)

    with open(book, "r") as f:
        lines = []
        line_count = 0
        for line in f:
            lines.append(line)
            line_count += 1
            if line_count > read_buffer_size:
                work_queue.put("\n".join(lines))
                lines = []
                line_count = 0

    # Poison the queue so the workers stop
    for x in worker_pool:
        work_queue.put(None)

    # Wait for the queue to empty
    work_queue.join()

    # pull from the output queue and reduce the data
    print(f"Top {top_x_words} most frequently seen words:")
    for word, value in most_common_words(results_queue, limit=top_x_words):
        print(f"\t{word}: {value}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("book")
    parser.add_argument("--num-workers", default=PROCESSOR_COUNT, type=int)
    parser.add_argument("--top-x", default=TOP_X_WORDS, type=int, dest="top_x_words")
    parser.add_argument("--read-buffer-size", default=READ_BUFFER_SIZE, type=int)
    args = parser.parse_args()
    print(args)

    start_work(args.book, args.num_workers, args.top_x_words, args.read_buffer_size)

