from wordcounter.wordcounter import tokenize, count_unique_words
import re

BOOK = "test/fixtures/book.txt"

SAMPLE_TEXT = """“There I object,” said Filby. “Of course a solid body may exist. All
real things—”

“So most people think. But wait a moment. Can an _instantaneous_ cube
exist?”

“Don’t follow you,” said Filby.

“Can a cube that does not last for any time at all, have a real
existence?”

Filby became pensive. “Clearly,” the Time Traveller proceeded, “any
real body must have extension in _four_ directions: it must have
Length, Breadth, Thickness, and—Duration. But through a natural
infirmity of the flesh, which I will explain to you in a moment, we
incline to overlook this fact. There are really four dimensions, three
which we call the three planes of Space, and a fourth, Time. There is,
however, a tendency to draw an unreal distinction between the former
three dimensions and the latter, because it happens that our
consciousness moves intermittently in one direction along the latter
from the beginning to the end of our lives.”"""


class TestWordCountWorker():
    def setUp(self):
        pass

    def test_tokenize(self):
        """Make sure we can split the text correctly and get the unique words"""
        tokens = tokenize(SAMPLE_TEXT)
        assert len(tokens) == 157
        for token in tokens:
            assert re.match(r"[a-zA-Z0-9]+", token), token

    def test_count_unique_words(self):
        """
        Given an array of unique words, make sure we see the right number of them
        """
        tokens = tokenize(SAMPLE_TEXT)
        unique_words = count_unique_words(tokens)
        assert unique_words
        assert unique_words["the"] == 8
        assert unique_words["time"] == 3  # Checking for caps/lowercase
